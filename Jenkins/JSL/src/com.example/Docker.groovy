!/usr/bin/env groovy 
package com.example 

class Docker implements Serializable (
    def script 
    Docker(script) {
        this.script = script 
    }
    def buildImage (String ImageName) {
        script.echo "Building Docker Image"
        script.sh "docker build -t $ImageName ."
    }

)