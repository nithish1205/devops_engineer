NB : C'est La version light des videos Graphana Adaptés pour la DGFIP w khw 

1 . Graphana UI Chnowa ? : Serveur Prometheus fih des metrics .. bch naccédiwlhom w nchoufouhom fi des dashboards lézmna outil de 
visualisation esmou hna Graphana 

2. Structure Général + Overview : Folder - Dashboards - Rows - Panels En effet :  
Hek 4 Morab3at - browse - yjiwék des dossiers fihom les dashboards li moujoudin kol - todkhol l dashboard : 
	- organisé sous forme plusieurs rows ( row ma3néha group de panels ( graphs ) yahkiw 3la nafs haja .. cpu , memoire , ram etc
..... ) 
	- Fama 3andk Time Range .. ( Data affiché pour les derniers 15 min , 30 min , 1 jour etc ..... ) .. tjm ta3ml custom time
	- Ki ténzél 3la panel - edit - tjik Query + transform - tal9a des queries maktoubin blangage esmo PromQL - Queries role mté3hom
Yextractiw data ml prometheus server - 
	- Fama browse Metric hano ta5tar Prometheus Metrics Manuellement 
	
3. Détéction d'Anomalies ( Exemples ) : 
	3.1 : Par exemple fama Dashboard responsable 3al suivi général du cluster .. Esmou CPU usage .. une fois l9it 
fih des spikes c'est que fama un pb -> VM héki fiha Machékl lézmk tsala7ha 
	3.2 : En générale ta93ad taba3 les Panels de Nodes ta3 cluster ( CPU , RAM ,Memoire etc .. ) .. lézm ykouno kol stables , rék7in , 
faméch des spikes etc .. 

4. Alerting : n7ébou win fama pb fl cluster tjina alerte on ne va pas passer tt la journée nka7lou 3al VMs . Par exemple si 
% CPU Usage > 50 % -> Wa9af 
Lien Des Alertes DGFIP Fl Prometheus : https://prometheuspcr.devops35.nubo.dgfip/prometheus/alerts
Enzél 3la alerte x .. yjiwék les champs : 
	- name : Esm l'alerte .. Lézm ykooun descriptif ll alerte 
	- expr : L'expression de Prometheus .. ( Langage PromQL ) 
Exemple : 
max_over_time(alertmanager_config_last_reload_succesful{job="monitoring-kube-prometheus-alertmanager" , namespace = "monitoring"}[5m]) == 0 

Décomposition : 
	alertmanager_config_last_reload_succesful  : Héthi L'Metric ta3 l'expression tnajém tal9aha fl UI ta3 Prometheus 
	(job ="..........",namespace =".....") : Filter 3al metric ... Fama baaaaaarcha mnhom 
	max_over_time : Prometheus Function .. Traja3lk le maximum fl intervalle de temps héka 
	[5m] : L'intervalle de temps li bch tlawj fih fonction max_over_time 
RQ : L'alerte héthi ne sera lancé que si max_over_time(alertmanager_config_last_reload_succesful
{job="monitoring-kube-prometheus-alertmanager" , namespace = "monitoring"}[5m]) = 0 ama dans le cas de nana il est égale à 1 
héka aléh l'alerte n'a jamais été active ( Jamais sarét ) 
	