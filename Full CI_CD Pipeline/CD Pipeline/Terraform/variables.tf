variable "vpc_cidr_block" {
    default = "10.0.0.0/16" 
}

variable "subnet_cidr_block" {
    default = "10.0.0.0/24" 
}

variable "env-prefix" {
    default = "dev"
}

variable "availability_zone" {
    default = "eu-west-3a"
}

variable "instance_type" {
    default = "t2.micro"
}

variable "region" {
    default = "eu-west-3"
} 