provider "aws" {region= var.region}

resource "aws_vpc" "myapp-vpc-1" {
    cidr_block =  var.vpc_cidr_block
    tags = { 
        Name : "${var.env-prefix}-vpc"
    }
}

resource "aws_subnet" "myapp-subnet-1" {
    vpc_id = aws_vpc.vpc-1.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.availability_zone
    tags = { 
        Name : "${var.env-prefix}-subnet"
    }
}

resource "aws_internet_gateway" "myapp-gateway" {
    vpc_id = aws_vpc.myapp-vpc-1.id
     tags = { 
        Name : "${var.env-prefix}-gateway"
    }
}

resource "aws_route_table" "myapp-route-table" {
    vpc_id = aws_vpc.myapp-vpc-1.id
    route = {
        cidr_block = "0.0.0.0/0" 
        gateway_id = aws_internet_gateway.myapp-gateway.id
    }
     tags = { 
        Name : "${var.env_prefix}-rte"
    }
}

resource "aws_route_table_association" "rtb-subnet" {
    subnet_id = aws_subnet.myapp-subnet-1.id
    route_table_id =  aws_route_table.myapp-route-table.id
}



resource "aws_security_group" "myapp-sg"  {
		name = "........" 
		vpc_id = aws_vpc.myvpc.id 
		ingress { 
			from_port = 22  
			to_port = 22 
			protocol = "tcp" 
			cidr_blocks = ["var.my_ip"]  
		}
		ingress {
			from_port = 8080 
			to_port  = 8080 
			protocol = "tcp" 
			cidr_blocks = ["0.0.0.0/0"]   
		}
		egress {
			from_port = 0  
			to_port = 0   
			protocol = "-1"	
			cidr_blocks =  ["0.0.0.0/0"]
			prefix_list_ids = []  
		}
	}



data "aws_ami" "myapp-ami" {
    most_recent = true 
    owners = ['amazon']
    filters {
        name : "name"
        values : ['reg-exp']
    }
    filters {
        name : "virtualization-type" 
        values : ['hvm']
    }
}



resource "aws_instance" "myapp-instance" {
    ami = data.aws_ami.myapp-ami.id 
    instace_type = var.instance_type 

    subnet_id = aws_subnet.myapp-subnet.id 
	vpc_security_group_ids = [aws_security_group.my-app-security-group.id ] # Voir RQ 5
	availability_zone = var.availability_zone # Voir RQ 6 

	associate_public_ip_adress = true # Voir RQ 7 
	key_name = "myapp-key-pair" 
    user_data = file('entry-script.sh')
	
    tags = {
		Name : "${var.env}-instance"
	}
   
}
