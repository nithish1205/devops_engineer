from kafka import KafkaProducer
import json
print('Step 1 : Creating Producer')
producer = KafkaProducer(
    bootstrap_servers = ['localhost:9092'] ,
    security_protocol = "SSH" ,
    value_serializer = lambda  value : json.dumps(value).encode('utf-8') ,
    key_serializer = lambda key : json.dumps(key).encode('utf-8') ,
    api_version=(2,0,2)
)
print('Step 2 : Creating Topic ')

from confluent_kafka.admin import AdminClient, NewTopic
admin_client = AdminClient({"bootstrap_servers": "localhost:9092"})


admin_client = AdminClient({"bootstrap_servers": "localhost:9092"})

exit()

print('Step 2 : Sending Messages')
producer.send (
    topic = 'SBH' ,
    key = 'Hobbie' ,
    value = 'Koura'
)
print('Step 3 : Flushing ')
producer.flush()
