
Demo : 
Introduction : 
1 . What is Kafka : ( Trivial : Zéyéd Nhotouh Voir Crush course ) 
2 . Pk Utiliser Apache Kafka : ( Reporté Inchallah )
	2.1 : RESTful Systems chma3néha ? + 2.2 : Common U.C 
	
TP : 	
Prequis : Dossier Ma7loul fl pycharm + Environnement Virtuelle activé Sabéb fih kafka-python
3 . Bch Yraka7 Environnement Kafka bl docker kiféh ? fama 2 images docker possible énou nétbaséw alihom : 
	- wurstmeister 
	- confluent ( Yhéblha 8 GB de RAM )
	Kiféh nrak7o l'env bl wurstmeister ? 
	
4. Etape 1 : Clonage : 
	cd Repo_ta3 projet # Question 1 : Thabét win fil cd hétha .. 
	git clone https://github.com/wurstmeister/kafka-docker.git
	cd kafka-docker 
	
5. Etape 2 : Yémchi ll docker-compose.yaml w ya3ml fih les modifications héthom :
	- Zéd version 3al image ( ként : image: wurstmeister/zookeeper  - walét image: wurstmeister/zookeeper:3.4.6 )
	- Na7a restart: unless-stopped
	- port radha "9092:9092" fi 3ou4 "9092" khw 
	- Zéd expose: # Question 2 : Chma3néha expose ? 
     		- "9093"
	- Les variables d'env na7ahom  kol sauf wahda ( KAFKA_ZOOKEEPER_CONNECT ) 
	- Zéd les variables suivants : 
		      KAFKA_ADVERTISED_LISTENERS: INSIDE://kafka:9093,OUTSIDE://localhost:9092
      		      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: INSIDE:PLAINTEXT,OUTSIDE:PLAINTEXT
      		      KAFKA_LISTENERS: INSIDE://0.0.0.0:9093,OUTSIDE://0.0.0.0:9092
      		      KAFKA_INTER_BROKER_LISTENER_NAME: INSIDE
	- na7a restart: unless-stopped flkher 
	RQ 1 : Ces modifs apportés t3amlo b tari9a tkhali ( Kafka - Zookeeper - Producer - Consumer ) Capable de communiquer 
	binét b3a4hom	
	RQ 2 : Zookeeper howa service responsable 3al Cluster Management 
	# Question 3 : Fi cas Réel kiféh na3ml héki rabtat éna ?
	
6. Etape 3 ( Optional ) : Na3mlou des modifications personalisés ll kafka fl docker-compose.yaml 
	6.1 : Une fois nhb npersonalisi des paramètres fl kafka ( e.g : message.max.bytes ) il suffit éni hal paramètre nassocilo 
	variable d'environnement 
	6.2 : Comment ? 
	Ex 1 : 
	Par exemple message.max.bytes devient 
	KAFKA_MESSAGE_MAX_BYTES : 20000
	6.3 : Par defaut une fois  producer sna3 event w yhb ypushih fi topic , topic héka yétsna3 wahdo wahdo automatiquement 
	mm si makénch moujoud 
	6.4 : Hal comportement de création automatique de topic est controllé par le paramètre auto.creates.topic.enable bch nbadloh
	némchi ll docker-compose w na3mlo : 
	KAFKA_AUTO_CREATE_TOPICS_ENABLE: 'false'
	# Question 4 : Majuscule Y2athar fl docker-compose ? 
	RQ : Wa9téli badélna fi variable d'environnement fl docker-compose .. Les images ma3awdouch tlanséw .. L'image li badéltlha variable d'environnement hano 3amlét kafka-docker_zookeeper_1 is up-to-date .. w lokhra li mamasinéhéch Recreating kafka-docker_kafka_1 ... done	
	
7. Etape 4 : Tlanciyét docker-compose.yaml & Test 
	7.1 : sudo docker-compose up -d ( Tanséch sudo , w Tanséch tétplaca wost kafka-docker bch ynajém ychouf docker-compose.yaml ) 
	7.2 : Test : sudo docker-compose ps
	
8. Etape 5 : San3an Producer yéb3a4 des events de differents type chaque 3 secondes 
	8.1 : Voir code producer.py
	8.1 : Raisonnement : Ahna nhbo nésta3mlo fonction producer.send (topic=topic , key=key , value=value ) donc lézmna n7a4ro
	les composants de cette fonction .. 
		# producer : import kafka .. KafkaProducer (.. ) 
		# send : fonction téb3a l objet 
		# topic : Il suffit T7a4ar Esmo ww t3adih .. Ataw yétsna3 Automatiquement Wa7do ( AFKA_AUTO_CREATE_TOPICS_ENABLE: 'true' fl docker-compose ) 
		# key,value : N7a4ro 2 fonction Ygénériw aléatoirement key,value 
		# While(True) : bch ya93ad ygénéri sns arret 
		# time.sleep(3) : Temps d'attente bin envoi w li ba3do 
		
RQ 1 : python3 producer.py & : Bch tlanci producer fl background .. 
RQ 2 : pkill -f producer.py : bch tnahi lancement mté3o fl background .. 

9. Etape 6 : San3an Consumer : 
	9.1 : Voir code consumer.py 
	9.2 : Raisonnement : ( Rien de spécial )
		# Nasna3 consumer
		# N9aydo fi topic ( najmo méli nasn3o nasn3o m9ayéd fi topic ki n3adi esm topic fl constructeur ta3 consumer ) 
		# Na9ra les messages ta3 producer 
		
10. Discutons ces paramètres : offset,auto_offset_reset,group_id

11. Etape 7 : Tba3wil kafka container : 
	11.1 : docker exec -it $(docker ps -aqf "name=kafka-docker_kafka") bash # Y7elk container ta3 kafka 
		RQ : $(docker ps -aqf "name=kafka-docker_kafka") traja3lk id kafka container 
	11.2 : cd /opt/bin/kafka + ls : Bch taffichilk des scripts sh yosl7o fl interaction m3a kafka server mte3ek
	11.3 : Exemple 1 : kafa-topics.sh --list --bootstrap-server kafka:9092 # Tlistilk les topics fl kafka cluster hétha 
	11.4 : Exemple 2 : kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic KHARJA --from-beginning 
		# Taffichilk les events li moujoudin lkol wost topic hétha w ml lowl ( --from-beginning ) 
12. Auto Commit ( Ba3d nchalh ) 
13. Partitions ( Ba3d nchalah ) 
	
Questions : 
1 / -aqf "name docker ps -aqf "name=kafka-docker_kafka" : fch ta3ml héki + La9tét dollar fil commande : docker exec  -it $(docker ps -aqf "name=kafka-docker_kafka") bash 









